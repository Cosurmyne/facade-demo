# Solution
Console application automating the processing of student assessment marks.  
This particular solution implements the **Facade pattern** on an ongoing project which had already implemented two patterns before it *(the Singleton, Command, and the Adapter patterns, respectively)*.
## Screenshot
![Screenshot](https://cloud.owncube.com/apps/gallery/preview.public/56245693?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)
![Screenshot](https://cloud.owncube.com/apps/gallery/preview.public/56359972?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)

The application implements the following **commands**.

1. view ITS
1. clear ITS
1. capture Marks
1. clear Marks
1. capture Sup
1. clear Sup
1. capture Student
1. clear Student

The first screenshot above, shows the user selection of **option 1**, viewing information in the ITS.  
The subsequent shows the selection of **option 3**, capturing marks to the ITS.

## UML

![Diagram](https://cloud.owncube.com/apps/gallery/preview.public/56359959?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)

## Implementation
### View ITS
Command Object	: ViewITS  
Function		: **Execute()**  
Receiver Object	: UI  
Invoker Object	: App
### Clear ITS
Command Object	: ViewITS  
Function		: **Undo()**  
Receiver Object	: UI  
Invoker			: App
### Capture Marks
Command Object	: CaptureMarks  
Function		: **Execute()**  
Receiver Object	: Database  
Invoker Object	: App
### Clear Marks
Command Object	: CaptureMarks  
Function		: **Undo()**  
Receiver Object	: Database  
Invoker Object	: App
### Capture Sup
Command Object	: CaptureMarks  
Function		: **Execute()**  
Receiver Object	: Database  
Invoker Object	: App
### Clear Sup
Command Object	: CaptureMarks  
Function		: **Undo()**  
Receiver Object	: Database  
Invoker Object	: App
### Capture Student
Command Object	: CaptureStudent  
Function		: **Execute()**  
Receiver Object	: UI  
Invoker Object	: App
### Clear Marks
Command Object	: CaptureStudent  
Function		: **Undo()**  
Receiver Object	: UI  
Invoker Object	: App