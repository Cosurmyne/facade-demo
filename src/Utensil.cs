using System;

namespace root
{
    static class Utensil
    {

        public static byte Pavilion()
        {
            string[] prompt = { "View ITS", "Clear ITS", "Capture Marks", "Clear Marks", "Capture Sup", "Clear Sup", "Capture Student", "Clear Student" };
            return Utensil.Prompt("ITS Plus Plus", prompt, true);
        }

        public static string Assessment(ref Int16 index)
        {
            string[] prompt = { "Moodle Quiz", "Practical", "Test", "Assignment" };
            index = (short)Utensil.Prompt("Please Select Assessment", prompt);
            return prompt[index - 1];
        }

        static byte Prompt(string prompt, string[] options, bool exit = false)
        {
            Int32 val = new Int32();
            do
            {
                Console.Clear();
                Console.WriteLine("{0}", prompt);
                Console.WriteLine("".PadRight(prompt.Length, '='));
                for (byte i = 1; i <= options.Length; i++) Console.WriteLine("- {0} :: {1}", i, options[i - 1]);
                Console.WriteLine();

                if (exit)
                {
                    Console.WriteLine("- {0} :: Exit", 0);
                    Console.WriteLine();
                }
                Console.Write("selection >> ");
                if (!IsInt(Console.ReadLine(), ref val)) continue;
                if (val >= (exit ? 0 : 1) && val <= options.Length) break;
            } while (true);
            return (byte)val;
        }
        public static bool IsInt(string input, ref Int32 val)
        {
            try
            {
                val = int.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
    }
}
