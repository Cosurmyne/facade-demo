﻿namespace root
{
    class Program
    {
        static void Main(string[] args)
        {
            Facade focus = new Facade();
            byte choice = Utensil.Pavilion();
            while (choice != 0)
            {
                switch (choice)
                {
                    case 1: focus.DisplayITS(); break;
                    case 2: focus.ClearITS(); break;
                    case 3: focus.CaptureMarks(); break;
                    case 4: focus.ClearMarks(); break;
                    case 5: focus.CaptureSup(); break;
                    case 6: focus.ClearSup(); break;
                    case 7: focus.CaptureStudent(); break;
                    case 8: focus.ClearStudent(); break;
                }
                choice = Utensil.Pavilion();
            }
        }
    }
}
