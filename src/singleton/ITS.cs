namespace root
{
    public sealed class ITS
    {
        private static ITS _instance;
        private static readonly object _lockThis = new object();

        private readonly Database[] _tm = new Database[5];

        public Database[] TM { get { return _tm; } }

        private ITS()
        {
            _tm[0] = new Database(1);
            _tm[1] = new Database(2);
            _tm[2] = new Database(3);
            _tm[3] = new Database(4);
            _tm[4] = new Database(5);
        }

        public static ITS GetITS()
        {
            lock (_lockThis)
            {
                if (_instance == null) _instance = new ITS();
            }
            return _instance;
        }

        public bool HasSup()
        {
            if (!Complete()) return false;
            byte? mark = Marks();
            if (mark < 45 || mark > 49) return false;
            return true;
        }

        public override string ToString()
        {
            string output = "";
            byte? mark = Marks();

            output += "----------------------------------\n";
            output += "| FIELD  | TM1 | TM2 | TM3 | TM4 |\n";
            output += "----------------------------------\n";
            output += "| Mark   | " + TM[0].Marks.ToString().PadRight(3) + " | " + TM[1].Marks.ToString().PadRight(3) + " | " + TM[2].Marks.ToString().PadRight(3) + " | " + TM[3].Marks.ToString().PadRight(3) + " |\n";
            output += "| Result | " + TM[0].Symbol() + "   | " + TM[1].Symbol() + "   | " + TM[2].Symbol() + "   | " + TM[3].Symbol() + "   |\n";
            output += "| DP     | " + DP(TM[0].Marks, 1).ToString().PadRight(3) + " | " + DP(TM[1].Marks, 2).ToString().PadRight(3) + " | " + DP(TM[2].Marks, 3).ToString().PadRight(3) + " | " + DP(TM[3].Marks, 4).ToString().PadRight(3) + " |\n";
            output += "----------------------------------\n";
            output += "\n";
            output += "Total Mark".PadRight(13) + ": " + mark + "\n";
            output += "Archievement".PadRight(13) + ": " + Archievement(mark) + "\n";

            if (!HasSup() || TM[4].Marks == null) return output;
            output += "\n";
            output += "Sup Mark".PadRight(13) + ": " + TM[4].Marks + "\n";
            output += "New Total".PadRight(13) + ": " + Marks(true) + "\n";

            return output;
        }

        private bool Complete()
        {
            bool good = true;
            if (TM[0].Marks == null) return false;
            if (TM[1].Marks == null) return false;
            if (TM[2].Marks == null) return false;
            if (TM[3].Marks == null) return false;
            return good;
        }
        private string Archievement(byte? mark)
        {
            if (!Complete()) return "Incomplete";
            if (mark >= 75) return "Distinction";
            if (mark >= 50) return "Pass";
            if (mark >= 45) return "Extra Sup";
            return "Fail";
        }
        private byte? Round(double? digit)
        {
            return digit == null ? null : (byte?)decimal.Round((decimal)digit);
        }
        private byte? DP(byte? mark, byte TM)
        {
            double[] weight = { .3, .3, .3, .1, .1 };
            return mark == null ? null : (byte?)decimal.Round((decimal)(mark * weight[TM - 1]));
        }
        private byte? Marks(bool sup = false)
        {
            double? total = .0;
            total += this.TM[0].Marks == null ? 0 : this.TM[0].Marks * .3;
            total += this.TM[1].Marks == null ? 0 : this.TM[1].Marks * .3;
            total += this.TM[2].Marks == null ? 0 : this.TM[2].Marks * .3;
            total += this.TM[3].Marks == null ? 0 : this.TM[3].Marks * .1;
            total += sup && this.TM[4].Marks != null ? this.TM[4].Marks * .1 : 0;
            return Round(total);
        }
    }
}
