namespace root
{
    public sealed class Student
    {
        private static Student _instance;
        private static readonly object _lockThis = new object();

        public string STDNum { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }

        private Student()
        {
		Reset();
        }

        public static Student GetStudent()
        {
            lock (_lockThis)
            {
                if (_instance == null) _instance = new Student();
            }
            return _instance;
        }

	public void Reset()
	{
            STDNum = "123456789";
            FName = "John";
            LName = "Doe";
	}

        public override string ToString()
        {
            return FName + " " + LName + " (" + STDNum + ")";
        }
    }
}
