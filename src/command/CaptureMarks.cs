namespace root
{
	class CaptureMarks : ICommand
	{
		public string Assessment { get; set; }
		protected Database _receiver;

		public CaptureMarks(Database receiver)
		{
			_receiver = receiver;
		}
		public void Execute()
		{
			_receiver.CaptureMarks(Assessment);
		}
		public void Undo()
		{
			_receiver.ClearMarks();
		}
	}
}
