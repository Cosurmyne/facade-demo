namespace root.adapter
{
    public class ITS : ITarget
    {
        public void DoStuff()
        {
            ViewITS command = new ViewITS(new UI());
            App invoker = new App(command);
            invoker.PushForward();
        }

        public void Renege()
        {
            ViewITS command = new ViewITS(new UI());
            App invoker = new App(command);
            invoker.RollBack();
        }
    }
}
