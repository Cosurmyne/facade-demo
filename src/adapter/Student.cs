namespace root.adapter
{
    class Student : ITarget
    {
        public void DoStuff()
        {
            CaptureStudent command = new CaptureStudent(new UI());
            App invoker = new App(command);
            invoker.PushForward();
        }

        public void Renege()
        {
            CaptureStudent command = new CaptureStudent(new UI());
            App invoker = new App(command);
            invoker.RollBack();
        }
    }
}
