namespace root.adapter
{
    class Marks
    {
        public void Capture(byte id, string assessment)
        {
            CaptureMarks command = new CaptureMarks(root.ITS.GetITS().TM[id - 1]);
            command.Assessment = assessment;
            App invoker = new App(command);
            invoker.PushForward();
        }
        public void Clear(byte id)
        {
            CaptureMarks command = new CaptureMarks(root.ITS.GetITS().TM[id - 1]);
            App invoker = new App(command);
            invoker.RollBack();
        }
    }
}
