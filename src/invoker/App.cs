namespace root
{
	public class App
	{
		private ICommand _command;

		public App(ICommand command)
		{
			this._command = command;
		}

		public void PushForward()
		{
			_command.Execute();
		}
		public void RollBack()
		{
			_command.Undo();
		}
	}
}
