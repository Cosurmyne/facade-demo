using System;

namespace root
{
    class Facade
    {
        private Adapter _marks;
        private adapter.ITS _its;
        private adapter.Student _student;
        private adapter.Sup _sup;

        public Facade()
        {
            _marks = new Adapter(new adapter.Marks());
            _its = new adapter.ITS();
            _student = new adapter.Student();
            _sup = new adapter.Sup();
        }

        public void DisplayITS()
        {
            _its.DoStuff();
        }
        public void ClearITS()
        {
            _its.Renege();
        }

        public void CaptureMarks()
        {
            Int16 val = new Int16();
            string selection = Utensil.Assessment(ref val);
            _marks.Id = (byte)val;
            _marks.Assessment = selection;
            _marks.DoStuff();
        }
        public void ClearMarks()
        {
            Int16 val = new Int16();
            Utensil.Assessment(ref val);
            _marks.Id = (byte)val;
            _marks.Renege();
        }

        public void CaptureSup()
        {
            if (!ITS.GetITS().HasSup())
            {
                Console.WriteLine("Student doesn't qualify for Sup");
                Console.ReadLine();
                return;
            }
            _sup.DoStuff();
        }
        public void ClearSup()
        {
            if (ITS.GetITS().TM[4].Marks == null)
            {
                Console.WriteLine("Student doesn't have Sup");
                Console.ReadLine();
                return;
            }
            _sup.Renege();
        }

        public void CaptureStudent()
        {
            _student.DoStuff();
        }
        public void ClearStudent()
        {
            _student.Renege();
        }
    }
}
